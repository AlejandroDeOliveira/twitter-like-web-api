﻿namespace ArchitectureTests
{
    public class ArchitectureTests
    {
        [Fact]
        public void Application_Should_Depend_On_Domain()
        {
            // Arrange
            var applicationAssembly = typeof(Application.Ports.IUserService).Assembly;
            var domainAssembly = typeof(Domain.User).Assembly;

            // Act
            var references = applicationAssembly.GetReferencedAssemblies();

            // Assert
            Assert.Contains(references, a => a.FullName == domainAssembly.FullName);
        }

        [Fact]
        public void Application_Should_Not_Depend_On_Api()
        {
            // Arrange
            var applicationAssembly = typeof(Application.Ports.IUserService).Assembly;
            var apiAssemblyName = "Api";

            // Act
            var references = applicationAssembly.GetReferencedAssemblies();

            // Assert
            Assert.DoesNotContain(references, a => a.Name == apiAssemblyName);
        }

        [Fact]
        public void Data_Should_Depend_On_Domain()
        {
            // Arrange
            var dataAssembly = typeof(Data.Adapters.UserRepository).Assembly;
            var domainAssembly = typeof(Domain.User).Assembly;

            // Act
            var references = dataAssembly.GetReferencedAssemblies();

            // Assert
            Assert.Contains(references, a => a.FullName == domainAssembly.FullName);
        }

        [Fact]
        public void Data_Should_Not_Depend_On_Api_Or_Application()
        {
            // Arrange
            var dataAssembly = typeof(Data.Adapters.UserRepository).Assembly;
            var apiAssemblyName = "Api";
            var applicationAssemblyName = typeof(Application.Ports.IUserService).Assembly.GetName().Name;

            // Act
            var references = dataAssembly.GetReferencedAssemblies();

            // Assert
            Assert.DoesNotContain(references, a => a.Name == apiAssemblyName);
            Assert.DoesNotContain(references, a => a.Name == applicationAssemblyName);
        }

        [Fact]
        public void Domain_Should_Not_Depend_On_Other_Layers()
        {
            // Arrange
            var domainAssembly = typeof(Domain.User).Assembly;
            var apiAssemblyName = "Api";
            var applicationAssemblyName = typeof(Application.Ports.IUserService).Assembly.GetName().Name;
            var dataAssemblyName = typeof(Data.Adapters.UserRepository).Assembly.GetName().Name;

            // Act
            var references = domainAssembly.GetReferencedAssemblies();

            // Assert
            Assert.DoesNotContain(references, a => a.Name == apiAssemblyName);
            Assert.DoesNotContain(references, a => a.Name == applicationAssemblyName);
            Assert.DoesNotContain(references, a => a.Name == dataAssemblyName);
        }
    }
}
