﻿namespace ApiIntegrationTests
{
    public class WallControllerIntegrationTest : IClassFixture<WebApplicationFactory<Api.Startup>>
    {
        private readonly WebApplicationFactory<Api.Startup> _factory;
        private readonly HttpClient _client;

        public WallControllerIntegrationTest(WebApplicationFactory<Api.Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task PostMessage_ShouldReturnOk_WhenUserExists()
        {
            // Arrange
            var username = "testuser";
            var userCreationDTO = new UserCreationDTO { Username = username };
            var messageDTO = new MessageDTO { Content = "Hello World", Timestamp = DateTime.UtcNow };

            // Add the user first
            var postResponse = await _client.PostAsJsonAsync("/User", userCreationDTO);
            postResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.PostAsJsonAsync($"/Wall/{username}/messages", messageDTO);

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task PostMessage_ShouldReturnNotFound_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";
            var messageDTO = new MessageDTO { Content = "Hello World", Timestamp = DateTime.UtcNow };

            // Act
            var response = await _client.PostAsJsonAsync($"/Wall/{username}/messages", messageDTO);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task FollowUser_ShouldReturnOk_WhenBothUsersExist()
        {
            // Arrange
            var followerUsername = "followeruser";
            var followeeUsername = "followeeuser";
            var followerCreationDTO = new UserCreationDTO { Username = followerUsername };
            var followeeCreationDTO = new UserCreationDTO { Username = followeeUsername };
            var followDTO = new FollowDTO { Username = followeeUsername };

            // Add both users first
            var followerPostResponse = await _client.PostAsJsonAsync("/User", followerCreationDTO);
            followerPostResponse.EnsureSuccessStatusCode();
            var followeePostResponse = await _client.PostAsJsonAsync("/User", followeeCreationDTO);
            followeePostResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.PostAsJsonAsync($"/Wall/{followerUsername}/follow", followDTO);

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task FollowUser_ShouldReturnNotFound_WhenFollowerUserDoesNotExist()
        {
            // Arrange
            var followerUsername = "nonexistentuser";
            var followeeUsername = "existinguser";
            var followeeCreationDTO = new UserCreationDTO { Username = followeeUsername };
            var followDTO = new FollowDTO { Username = followeeUsername };

            // Add the followee user first
            var followeePostResponse = await _client.PostAsJsonAsync("/User", followeeCreationDTO);
            followeePostResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.PostAsJsonAsync($"/Wall/{followerUsername}/follow", followDTO);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task FollowUser_ShouldReturnNotFound_WhenFolloweeUserDoesNotExist()
        {
            // Arrange
            var followerUsername = "existinguser";
            var followeeUsername = "nonexistentuser";
            var followerCreationDTO = new UserCreationDTO { Username = followerUsername };
            var followDTO = new FollowDTO { Username = followeeUsername };

            // Add the follower user first
            var followerPostResponse = await _client.PostAsJsonAsync("/User", followerCreationDTO);
            followerPostResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.PostAsJsonAsync($"/Wall/{followerUsername}/follow", followDTO);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetWall_ShouldReturnWallDTO_WhenUserExists()
        {
            // Arrange
            var username = "testuser";
            var userCreationDTO = new UserCreationDTO { Username = username };

            // Add the user first
            var postResponse = await _client.PostAsJsonAsync("/User", userCreationDTO);
            postResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.GetAsync($"/Wall/{username}");

            // Assert
            response.EnsureSuccessStatusCode();
            var wallDTO = await response.Content.ReadFromJsonAsync<WallDTO>();
            Assert.NotNull(wallDTO);
        }

        [Fact]
        public async Task GetWall_ShouldReturnNotFound_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";

            // Act
            var response = await _client.GetAsync($"/Wall/{username}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
