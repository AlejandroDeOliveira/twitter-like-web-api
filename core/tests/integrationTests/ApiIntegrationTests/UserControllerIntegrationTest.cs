﻿namespace ApplicationIntegrationTests
{
    public class UserControllerIntegrationTest : IClassFixture<WebApplicationFactory<Api.Startup>>
    {
        private readonly WebApplicationFactory<Api.Startup> _factory;
        private readonly HttpClient _client;

        public UserControllerIntegrationTest(WebApplicationFactory<Api.Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task GetUser_ShouldReturnUserDTO_WhenUserExists()
        {
            // Arrange
            var username = "testuser";
            var userCreationDTO = new UserCreationDTO { Username = username };

            // Add the user first
            var postResponse = await _client.PostAsJsonAsync("/User", userCreationDTO);
            postResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.GetAsync($"/User/{username}");

            // Assert
            response.EnsureSuccessStatusCode();
            var userDTO = await response.Content.ReadFromJsonAsync<UserDTO>();
            Assert.NotNull(userDTO);
            Assert.Equal(username, userDTO.Username);
        }

        [Fact]
        public async Task GetUser_ShouldReturnNotFound_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";

            // Act
            var response = await _client.GetAsync($"/User/{username}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task AddUser_ShouldReturnCreatedAtAction()
        {
            // Arrange
            var username = "newuser";
            var userCreationDTO = new UserCreationDTO { Username = username };

            // Act
            var response = await _client.PostAsJsonAsync("/User", userCreationDTO);

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            var createdUser = await response.Content.ReadFromJsonAsync<UserCreationDTO>();
            Assert.NotNull(createdUser);
            Assert.Equal(username, createdUser.Username);
        }

        [Fact]
        public async Task EditUser_ShouldReturnEditedUserDTO_WhenUserIsEdited()
        {
            // Arrange
            var username = "existinguser";
            var userCreationDTO = new UserCreationDTO { Username = username };
            var userEditionDTO = new UserEditionDTO { newUsername = "editeduser" };

            // Add the user first
            var postResponse = await _client.PostAsJsonAsync("/User", userCreationDTO);
            postResponse.EnsureSuccessStatusCode();

            // Act
            var response = await _client.PutAsJsonAsync($"/User/{username}", userEditionDTO);

            // Assert
            response.EnsureSuccessStatusCode();
            var editedUserDTO = await response.Content.ReadFromJsonAsync<UserDTO>();
            Assert.NotNull(editedUserDTO);
            Assert.Equal(userEditionDTO.newUsername, editedUserDTO.Username);
        }

        [Fact]
        public async Task EditUser_ShouldReturnNotFound_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";
            var userEditionDTO = new UserEditionDTO { newUsername = "editeduser" };

            // Act
            var response = await _client.PutAsJsonAsync($"/User/{username}", userEditionDTO);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
