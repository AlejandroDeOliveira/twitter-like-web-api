﻿namespace ApplicationUnitTests
{
    public class UserServiceUnitTest
    {
        private readonly UserRepositoryMock _fakeUserRepository;
        private readonly UserService _userService;

        public UserServiceUnitTest()
        {
            _fakeUserRepository = new UserRepositoryMock();
            _userService = new UserService(_fakeUserRepository);
        }

        [Fact]
        public void GetUserByUsername_ShouldReturnUserDTO_WhenUserExists()
        {
            // Arrange
            var username = "testuser";
            var user = new User(username);
            _fakeUserRepository.Users.Add(user);

            // Act
            var result = _userService.GetUserByUsername(username);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(username, result.Username);
        }

        [Fact]
        public void GetUserByUsername_ShouldReturnNull_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";

            // Act
            var result = _userService.GetUserByUsername(username);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void AddUser_ShouldCallRepositoryAddUser()
        {
            // Arrange
            var userCreationDTO = new UserCreationDTO { Username = "newuser" };

            // Act
            _userService.AddUser(userCreationDTO);

            // Assert
            var addedUser = _fakeUserRepository.GetByUsername(userCreationDTO.Username);
            Assert.NotNull(addedUser);
            Assert.Equal(userCreationDTO.Username, addedUser.Username);
        }

        [Fact]
        public void EditUser_ShouldReturnEditedUserDTO_WhenUserIsEdited()
        {
            // Arrange
            var username = "existinguser";
            var userEditionDTO = new UserEditionDTO { newUsername = "editeduser" };
            var user = new User(username);
            _fakeUserRepository.Users.Add(user);

            // Act
            var result = _userService.EditUser(username, userEditionDTO);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(userEditionDTO.newUsername, result.Username);
        }

        [Fact]
        public void EditUser_ShouldReturnNull_WhenUserIsNotFound()
        {
            // Arrange
            var username = "nonexistentuser";
            var userEditionDTO = new UserEditionDTO { newUsername = "editeduser" };

            // Act
            var result = _userService.EditUser(username, userEditionDTO);

            // Assert
            Assert.Null(result);
        }
    }
}
