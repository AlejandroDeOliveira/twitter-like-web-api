﻿namespace ApplicationUnitTests
{
    public class WallServiceUnitTest
    {
        private readonly UserRepositoryMock _fakeUserRepository;
        private readonly WallService _wallService;

        public WallServiceUnitTest()
        {
            _fakeUserRepository = new UserRepositoryMock();
            _wallService = new WallService(_fakeUserRepository);
        }

        [Fact]
        public void PostMessage_ShouldCallRepositoryEditUser_WhenUserExists()
        {
            // Arrange
            var username = "testuser";
            var user = new User(username);
            _fakeUserRepository.Users.Add(user);
            var userDTO = new UserDTO { Username = username };
            var content = "Hello, world!";
            var timestamp = DateTime.Now;

            // Act
            _wallService.PostMessage(userDTO, content, timestamp);

            // Assert
            var editedUser = _fakeUserRepository.GetByUsername(username);
            Assert.NotNull(editedUser);
            Assert.Contains(editedUser.Wall.Messages, message => message.Content == content && message.Timestamp == timestamp);
        }

        [Fact]
        public void PostMessage_ShouldNotCallRepositoryEditUser_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";
            var userDTO = new UserDTO { Username = username };
            var content = "Hello, world!";
            var timestamp = DateTime.Now;

            // Act
            _wallService.PostMessage(userDTO, content, timestamp);

            // Assert
            var user = _fakeUserRepository.GetByUsername(username);
            Assert.Null(user);
        }

        [Fact]
        public void FollowUser_ShouldCallRepositoryEditUser_WhenBothUsersExist()
        {
            // Arrange
            var followerUsername = "follower";
            var followeeUsername = "followee";
            var follower = new User(followerUsername);
            var followee = new User(followeeUsername);
            _fakeUserRepository.Users.Add(follower);
            _fakeUserRepository.Users.Add(followee);
            var followerDTO = new UserDTO { Username = followerUsername };
            var followeeDTO = new UserDTO { Username = followeeUsername };

            // Act
            _wallService.FollowUser(followerDTO, followeeDTO);

            // Assert
            var editedFollower = _fakeUserRepository.GetByUsername(followerUsername);
            Assert.NotNull(editedFollower);
            Assert.Contains(editedFollower.Wall.Following, followee => followee.Username == followeeUsername);
        }

        [Fact]
        public void FollowUser_ShouldNotCallRepositoryEditUser_WhenFollowerDoesNotExist()
        {
            // Arrange
            var followerUsername = "nonexistentfollower";
            var followeeUsername = "followee";
            var followee = new User(followeeUsername);
            _fakeUserRepository.Users.Add(followee);
            var followerDTO = new UserDTO { Username = followerUsername };
            var followeeDTO = new UserDTO { Username = followeeUsername };

            // Act
            _wallService.FollowUser(followerDTO, followeeDTO);

            // Assert
            var editedFollower = _fakeUserRepository.GetByUsername(followerUsername);
            Assert.Null(editedFollower);
        }

        [Fact]
        public void FollowUser_ShouldNotCallRepositoryEditUser_WhenFolloweeDoesNotExist()
        {
            // Arrange
            var followerUsername = "follower";
            var followeeUsername = "nonexistentfollowee";
            var follower = new User(followerUsername);
            _fakeUserRepository.Users.Add(follower);
            var followerDTO = new UserDTO { Username = followerUsername };
            var followeeDTO = new UserDTO { Username = followeeUsername };

            // Act
            _wallService.FollowUser(followerDTO, followeeDTO);

            // Assert
            var editedFollower = _fakeUserRepository.GetByUsername(followerUsername);
            Assert.NotNull(editedFollower);
            Assert.DoesNotContain(editedFollower.Wall.Following, followee => followee.Username == followeeUsername);
        }

        [Fact]
        public void GetWall_ShouldReturnWallDTOWithFollowingUsersAndMessages_WhenUserExists()
        {
            // Arrange
            var username = "testuser";
            var followeeUsername = "followee";
            var user = new User(username);
            var followee = new User(followeeUsername);
            user.Follow(followeeUsername);
            _fakeUserRepository.Users.Add(user);
            _fakeUserRepository.Users.Add(followee);
            var userDTO = new UserDTO { Username = username };

            // Act
            var result = _wallService.GetWall(userDTO);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result.Following);
            Assert.Equal(followeeUsername, result.Following.First().Username);
        }

        [Fact]
        public void GetWall_ShouldReturnNull_WhenUserDoesNotExist()
        {
            // Arrange
            var username = "nonexistentuser";
            var userDTO = new UserDTO { Username = username };

            // Act
            var result = _wallService.GetWall(userDTO);

            // Assert
            Assert.Null(result);
        }
    }
}
