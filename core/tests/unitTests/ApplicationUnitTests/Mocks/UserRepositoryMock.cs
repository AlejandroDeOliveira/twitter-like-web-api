﻿namespace ApplicationUnitTests.Mocks
{
    public class UserRepositoryMock : IUserRepository
    {
        public List<User> Users { get; } = new List<User>();

        public User GetByUsername(string username)
        {
            return Users.Find(user => user.Username == username);
        }

        public void AddUser(User user)
        {
            Users.Add(user);
        }

        public User EditUser(string username, User user)
        {
            var existingUser = GetByUsername(username);
            if (existingUser != null)
            {
                Users.Remove(existingUser);
                Users.Add(user);
                return user;
            }
            return null;
        }
    }
}
