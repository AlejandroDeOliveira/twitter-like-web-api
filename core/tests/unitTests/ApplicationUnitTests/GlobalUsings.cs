global using Xunit;
global using Application.Adapters;
global using Application.DTOs;
global using Data.Ports;
global using Domain;
global using ApplicationUnitTests.Mocks;