﻿
using System.Text;

namespace Domain
{
    public class Wall
    {
        public List<Message> Messages { get; set; }
        public List<User> Following { get; set; }

        public Wall ()
        {
            Messages = new List<Message> ();
            Following = new List<User> ();
        }


        public void PostMessage(Message message)
        {
            Messages.Add(message);
        }

        public void FollowUser(User user)
        {
            if (!Following.Contains(user))
            {
                Following.Add(user);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Following:");
            foreach (var user in Following)
            {
                sb.AppendLine(user.ToString());
            }

            sb.AppendLine("Messages:");
            foreach (var message in Messages)
            {
                sb.AppendLine(message.ToString());
            }

            return sb.ToString();
        }
    }
}
