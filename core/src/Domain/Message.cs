﻿namespace Domain
{
    public class Message
    {
        public string Content { get; private set; }
        public DateTime Timestamp { get; private set; }

        public Message(string content, DateTime timestamp)
        {
            Content = content;
            Timestamp = timestamp;
        }

        public override string ToString()
        {
            return $"{Timestamp}: {Content}";
        }
    }
}
