﻿namespace Domain
{
    public class User
    {
        public string Username { get; set; }
        public Wall Wall { get; set; }

        public User(string username)
        {
            Username = username;
            Wall = new Wall();
        }

        public void PostMessage(string content, DateTime timestamp)
        {
            Message message = new Message(content, timestamp);
            Wall.PostMessage(message);
        }

        public void Follow(string username)
        {
            User user = new User(username);
            Wall.FollowUser(user);
        }

        public override string ToString()
        {
            return $"Username: {Username}";
        }
    }
}
