﻿using Domain;

namespace Data.Ports
{
    public interface IUserRepository
    {
        User GetByUsername(string username);
        void AddUser(User user);
        User EditUser(string username, User user);
    }
}
