﻿using Data.Ports;
using Domain;

namespace Data.Adapters
{
    public class UserRepository : IUserRepository
    {
        private readonly List<User> _users = new List<User>();

        public UserRepository()
        {
            PopulateTestData();
        }

        public User GetByUsername(string username)
        {
            return _users.FirstOrDefault(u => u.Username == username);
        }

        public void AddUser(User user)
        {
            var existingUser = GetByUsername(user.Username);
            if (existingUser == null)
            {
                _users.Add(user);
            }
        }

        public User EditUser(string username, User user)
        {
            var existingUser = GetByUsername(username);
            if (existingUser != null)
            {
                int index = _users.FindIndex(u => u.Username == user.Username);

                existingUser.Wall = user.Wall;
                existingUser.Username = user.Username;

                if (index != -1)
                {
                    _users[index] = existingUser;
                }

                return existingUser;
            }

            return null;
        }

        private void PopulateTestData()
        {
            var alice = new User("alice") { Wall = new Wall() };
            var bob = new User("bob") { Wall = new Wall() };
            var charlie = new User("charlie") { Wall = new Wall() };
            var xxx = new User("xxx") { Wall = new Wall() };

            AddUser(alice);
            AddUser(bob);
            AddUser(charlie);
            AddUser(xxx);

            alice.PostMessage("Hello, world!", DateTime.Now.AddMinutes(-15));
            alice.PostMessage("It's a sunny day!", DateTime.Now.AddMinutes(-10));

            bob.PostMessage("I love programming!", DateTime.Now.AddMinutes(-20));
            bob.PostMessage("C# is awesome!", DateTime.Now.AddMinutes(-5));

            charlie.PostMessage("Just started learning hexagonal architecture.", DateTime.Now.AddMinutes(-25));
            charlie.PostMessage("Looking for good resources on SOLID principles.", DateTime.Now.AddMinutes(-12));

            xxx.PostMessage("heyyy1", DateTime.Now);
            xxx.PostMessage("heyyy2", DateTime.Now);

            alice.Follow("bob");
            alice.Follow("charlie");
            bob.Follow("alice");
            charlie.Follow("bob");

            EditUser("alice", alice);
            EditUser("bob", bob);
            EditUser("charlie", charlie);
            EditUser("xxx", xxx);
        }
    }
}
