﻿using Application.DTOs;
using Application.Ports;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WallController : ControllerBase
    {
        private readonly IWallService _wallService;
        private readonly IUserService _userService;

        public WallController(IWallService wallService, IUserService userService)
        {
            _wallService = wallService;
            _userService = userService;
        }

        [HttpPost("{username}/messages")]
        public ActionResult PostMessage(string username, [FromBody] MessageDTO messageDTO)
        {
            var userDTO = _userService.GetUserByUsername(username);
            if (userDTO == null)
            {
                return NotFound();
            }

            _wallService.PostMessage(userDTO, messageDTO.Content, messageDTO.Timestamp);
            return Ok();
        }

        [HttpPost("{username}/follow")]
        public ActionResult FollowUser(string username, [FromBody] FollowDTO followDTO)
        {
            var followerDTO = _userService.GetUserByUsername(username);
            var followeeDTO = _userService.GetUserByUsername(followDTO.Username);
            if (followerDTO == null || followeeDTO == null)
            {
                return NotFound();
            }

            _wallService.FollowUser(followerDTO, followeeDTO);
            return Ok();
        }

        [HttpGet("{username}")]
        public ActionResult<WallDTO> GetWall(string username)
        {
            var userDTO = _userService.GetUserByUsername(username);
            if (userDTO == null)
            {
                return NotFound();
            }

            var wallDTO = _wallService.GetWall(userDTO);
            return Ok(wallDTO);
        }
    }
}
