﻿using Application.DTOs;
using Application.Ports;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{username}")]
        public async Task<IActionResult> GetUser(string username)
        {
            var userDTO = _userService.GetUserByUsername(username);
            if (userDTO == null)
            {
                return NotFound();
            }
            return Ok(userDTO);
        }

        [HttpPost]
        public ActionResult AddUser([FromBody] UserCreationDTO userCreationDTO)
        {
            _userService.AddUser(userCreationDTO);
            return CreatedAtAction(nameof(GetUser), new { username = userCreationDTO.Username }, userCreationDTO);
        }

        [HttpPut("{username}")]
        public async Task<IActionResult> EditUser(string username, [FromBody] UserEditionDTO userEditionDTO)
        {
            var editedUserDTO = _userService.EditUser(username, userEditionDTO);
            if (editedUserDTO == null)
            {
                return NotFound();
            }
            return Ok(editedUserDTO);
        }
    }
}
