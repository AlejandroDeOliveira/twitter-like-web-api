﻿using Application.Adapters;
using Application.Ports;
using Data.Adapters;
using Data.Ports;

namespace Api
{
    public class Startup
    {
        public static WebApplication AppInitialize(String[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            ConfigureServices(builder);
            var _app = builder.Build();
            Configure(_app);
            return _app;
        }

        public static void ConfigureServices(WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddScoped<IWallService, WallService>();

            builder.Services.AddSingleton<IUserRepository, UserRepository>();

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddCors();
            builder.Services.AddControllers();
        }

        private static void Configure(WebApplication app)
        {
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            app.UseHttpsRedirection();
            //app.UseAuthorization();
            app.MapControllers();

            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            });
        }
    }
}
