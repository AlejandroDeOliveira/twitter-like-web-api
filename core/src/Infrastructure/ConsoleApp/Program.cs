﻿using ConsoleApp.Services;

class Program
{
    static async Task Main(string[] args)
    {
        var apiService = new ApiService();
        var userService = new UserService(apiService);

        while (true)
        {
            Console.Write("> ");
            var input = Console.ReadLine();
            var commandParts = input.Split(' ');
            var command = commandParts[0];

            switch (command)
            {
                case "post":
                    if (commandParts.Length < 3)
                    {
                        Console.WriteLine("Usage: post @username message content");
                        break;
                    }
                    var username = commandParts[1].TrimStart('@');
                    var messageContent = string.Join(' ', commandParts.Skip(2));
                    await userService.PostMessage(username, messageContent);
                    Console.WriteLine($"{username} posted -> \"{messageContent}\"");
                    Console.WriteLine();
                    break;

                case "follow":
                    if (commandParts.Length < 3)
                    {
                        Console.WriteLine("Usage: follow @followerUsername @followeeUsername");
                        break;
                    }
                    var followerUsername = commandParts[1].TrimStart('@');
                    var followeeUsername = commandParts[2].TrimStart('@');
                    await userService.FollowUser(followerUsername, followeeUsername);
                    Console.WriteLine($"{followerUsername} started following {followeeUsername}");
                    Console.WriteLine();
                    break;

                case "wall":
                    if (commandParts.Length < 2)
                    {
                        Console.WriteLine("Usage: wall @username");
                        break;
                    }
                    var dashboardUsername = commandParts[1].TrimStart('@');
                    var followees = await userService.GetDashboard(dashboardUsername);

                    foreach (var followee in followees.Following)
                    {
                        foreach (var message in followee.Messages)
                        {
                            Console.WriteLine($"\t\"{message.Content}\" @{followee.Username} @{message.Timestamp}");
                        }
                        Console.WriteLine();
                    }
                    break;

                default:
                    Console.WriteLine("Unknown command.");
                    break;
            }
        }
    }
}