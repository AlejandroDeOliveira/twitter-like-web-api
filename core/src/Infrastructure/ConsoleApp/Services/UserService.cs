﻿using Application.DTOs;
using System.Text.Json;

namespace ConsoleApp.Services
{
    public class UserService
    {
        private readonly ApiService _apiService;

        public UserService(ApiService apiService)
        {
            _apiService = apiService;
        }

        public async Task PostMessage(string username, string content)
        {
            var messageDTO = new MessageItem
            {
                Content = content,
                Timestamp = DateTime.UtcNow
            };

            await _apiService.PostAsync($"Wall/{username}/messages", messageDTO);
        }

        public async Task FollowUser(string followerUsername, string followeeUsername)
        {
            var followDTO = new FollowingItem
            {
                Username = followeeUsername
            };

            await _apiService.PostAsync($"Wall/{followerUsername}/follow", followDTO);
        }

        public async Task<WallResponse> GetDashboard(string username)
        {
            var response = await _apiService.GetAsync($"Wall/{username}");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                };

                var wallJson = JsonSerializer.Deserialize<WallResponse>(json, options);

                // Map wallJson to wallResponse
                var wallResponse = new WallResponse
                {
                    Following = wallJson.Following.Select(f => new FollowingItem
                    {
                        Username = f.Username,
                        Messages = f.Messages.Select(m => new MessageItem
                        {
                            Content = m.Content,
                            Timestamp = m.Timestamp
                        }).ToList()
                    }).ToList()
                };

                return wallResponse;
            }
            else
            {
                return null;
            }
        }
    }
}
