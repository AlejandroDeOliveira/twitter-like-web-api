﻿namespace Application.DTOs
{
    public class FollowingItem
    {
        public string Username { get; set; }
        public List<MessageItem> Messages { get; set; }
    }
}
