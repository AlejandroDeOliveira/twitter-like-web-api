﻿namespace Application.DTOs
{
    public class WallResponse
    {
        public List<FollowingItem> Following { get; set; }
    }
}
