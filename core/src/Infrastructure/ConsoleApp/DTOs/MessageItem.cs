﻿namespace Application.DTOs
{
    public class MessageItem
    {
        public string Content { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
