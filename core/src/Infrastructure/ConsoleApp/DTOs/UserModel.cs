﻿namespace Application.DTOs
{
    public class UserModel
    {
        public string Username { get; set; }
        public List<MessageItem> Messages { get; set; }
        public List<string> Following { get; set; }
    }

    public class UserCreationModel
    {
        public string Username { get; set; }
    }
}
