﻿using Application.DTOs;
using Application.Mappers;
using Application.Ports;
using Data.Ports;

namespace Application.Adapters
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserDTO GetUserByUsername(string username)
        {
            var user = _userRepository.GetByUsername(username);
            return user != null ? UserMapper.DomainToUserDTO(user) : null;
        }

        public void AddUser(UserCreationDTO userCreationDTO)
        {
            var user = UserMapper.UserCreationDTOToDomain(userCreationDTO);
            _userRepository.AddUser(user);
        }

        public UserDTO EditUser(string username, UserEditionDTO userEditionDTO)
        {
            var user = UserMapper.UserEditionDTOToDomain(userEditionDTO);
            var editedUser = _userRepository.EditUser(username, user);
            return editedUser != null ? UserMapper.DomainToUserDTO(editedUser) : null;
        }
    }
}
