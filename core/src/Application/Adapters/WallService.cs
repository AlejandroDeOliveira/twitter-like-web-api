﻿using Application.DTOs;
using Application.Mappers;
using Application.Ports;
using Data.Ports;

namespace Application.Adapters
{
    public class WallService : IWallService
    {
        private readonly IUserRepository _userRepository;

        public WallService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void PostMessage(UserDTO userDTO, string content, DateTime timestamp)
        {
            var user = _userRepository.GetByUsername(userDTO.Username);
            if (user != null)
            {
                user.PostMessage(content, timestamp);
                _userRepository.EditUser(userDTO.Username, user);
            }
        }

        public void FollowUser(UserDTO followerDTO, UserDTO followeeDTO)
        {
            var follower = _userRepository.GetByUsername(followerDTO.Username);
            var followee = _userRepository.GetByUsername(followeeDTO.Username);
            if (follower != null && followee != null)
            {
                follower.Follow(followee.Username);
                _userRepository.EditUser(followerDTO.Username, follower);
            }
        }

        public WallDTO GetWall(UserDTO userDTO)
        {
            var user = _userRepository.GetByUsername(userDTO.Username);
            if (user == null)
            {
                return null;
            }

            var followingUsers = user.Wall.Following
                                      .Select(followee => _userRepository.GetByUsername(followee.Username))
                                      .Where(followingUser => followingUser != null)
                                      .ToList();

            var wallDTO = new WallDTO
            {
                Following = followingUsers.Select(UserMapper.DomainToFolloweeDTO).ToList()
            };

            return wallDTO;
        }
    }
}