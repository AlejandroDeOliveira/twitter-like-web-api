﻿using Application.DTOs;
using Domain;

namespace Application.Ports
{
    public interface IWallService
    {
        void PostMessage(UserDTO userDTO, string content, DateTime timestamp);
        void FollowUser(UserDTO followerDTO, UserDTO followeeDTO);
        WallDTO GetWall(UserDTO userDTO);
    }
}
