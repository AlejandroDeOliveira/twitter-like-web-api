﻿using Application.DTOs;
using Domain;

namespace Application.Ports
{
    public interface IUserService
    {
        UserDTO GetUserByUsername(string username);
        void AddUser(UserCreationDTO userCreationDTO);
        UserDTO EditUser(string username, UserEditionDTO userEditionDTO);
    }
}
