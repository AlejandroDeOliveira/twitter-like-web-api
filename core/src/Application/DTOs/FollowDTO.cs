﻿namespace Application.DTOs
{
    public class FollowDTO
    {
        public string Username { get; set; }
    }

    public class FolloweeDTO
    {
        public string Username { get; set; }
        public List<MessageDTO> Messages { get; set; }
    }
}
