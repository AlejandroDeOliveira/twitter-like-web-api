﻿namespace Application.DTOs
{
    public class UserDTO
    {
        public string Username { get; set; }
        public List<MessageDTO> Messages { get; set; }
        public List<string> Following { get; set; }
    }

    public class UserCreationDTO
    {
        public string Username { get; set; }
    }

    public class UserEditionDTO
    {
        public string newUsername { get; set; }
    }
}
