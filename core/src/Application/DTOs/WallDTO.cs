﻿namespace Application.DTOs
{
    public class WallDTO
    {
        public List<FolloweeDTO> Following { get; set; }
    }
}
