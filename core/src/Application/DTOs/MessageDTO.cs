﻿namespace Application.DTOs
{
    public class MessageDTO
    {
        public string Content { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
