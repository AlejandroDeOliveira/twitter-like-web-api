﻿using Application.DTOs;
using Domain;

namespace Application.Mappers
{
    public class WallMapper
    {
        public static WallDTO ToDTO(Wall wall)
        {
            return new WallDTO
            {
                Following = wall.Following.Select(UserMapper.DomainToFolloweeDTO).ToList()
            };
        }

        public static Wall ToDomain(WallDTO wallDTO)
        {
            var wall = new Wall
            {
                Following = wallDTO.Following.Select(UserMapper.FolloweeDTOToDomain).ToList()
            };
            return wall;
        }
    }
}
