﻿using Application.DTOs;
using Domain;

namespace Application.Mappers
{
    public class MessageMapper
    {
        public static MessageDTO ToDTO(Message message)
        {
            return new MessageDTO
            {
                Content = message.Content,
                Timestamp = message.Timestamp
            };
        }

        public static Message ToDomain(MessageDTO messageDTO)
        {
            return new Message(messageDTO.Content, messageDTO.Timestamp);
        }
    }
}
