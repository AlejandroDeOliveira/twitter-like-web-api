﻿using Application.DTOs;
using Domain;

namespace Application.Mappers
{
    public class UserMapper
    {
        public static UserDTO DomainToUserDTO(User user)
        {
            return new UserDTO
            {
                Username = user.Username,
                Messages = user.Wall.Messages.Select(MessageMapper.ToDTO).ToList(),
                Following = user.Wall.Following.Select(u => u.Username).ToList()
            };
        }

        public static User UserDTOToDomain(UserDTO userDTO)
        {
            var user = new User(userDTO.Username)
            {
                Wall = new Wall
                {
                    Messages = userDTO.Messages.Select(MessageMapper.ToDomain).ToList(),
                    Following = userDTO.Following.Select(username => new User(username)).ToList()
                }
            };
            return user;
        }

        public static FolloweeDTO DomainToFolloweeDTO(User user)
        {
            return new FolloweeDTO
            {
                Username = user.Username,
                Messages = user.Wall.Messages.Select(MessageMapper.ToDTO).ToList(),
            };
        }

        public static User FolloweeDTOToDomain(FolloweeDTO followeeDTO)
        {
            var user = new User(followeeDTO.Username)
            {
                Wall = new Wall
                {
                    Messages = followeeDTO.Messages.Select(MessageMapper.ToDomain).ToList(),
                }
            };
            return user;
        }

        public static User UserCreationDTOToDomain(UserCreationDTO userCreationDTO)
        {
            var user = new User(userCreationDTO.Username);
            return user;
        }

        public static User UserEditionDTOToDomain(UserEditionDTO userEditionDTO)
        {
            var user = new User(userEditionDTO.newUsername);
            return user;
        }
    }
}
