# Twitter-like-web-api
This project is a web API that mimics the core functionalities of Twitter, allowing users to post messages, follow other users, and view posts from those they follow. It aims to provide a simplified yet robust platform for users to engage in a social networking environment through a RESTful API, facilitating a seamless experience for developers and users alike.

## Introduction
The Twitter-like-web-api project is designed to provide a simplified version of Twitter's core functionalities through a RESTful API. The goal is to offer a platform where users can post short messages, follow other users, and view a feed of posts from the users they follow. This project is intended for educational purposes, to demonstrate the implementation of a web API using C# and .NET, and to provide a foundation that can be expanded with more features and capabilities.

The project uses the principles of Hexagonal Architecture (also known as Ports and Adapters Architecture) to ensure a clean separation of concerns and maintainable codebase. This architectural style allows the core business logic to be isolated from external interfaces, making the application more modular, testable, and adaptable to changes.

## Features
The Twitter-like-web-api includes the following features:

- User Registration and Login: Users can create accounts and log in to access the platform.
- Posting Messages: Users can post short messages that are stored and timestamped.
- Following Users: Users can follow other users to see their posts in their feed.
- Viewing Feed: Users can view posts from the users they follow in a chronological feed.

## Installation
Step-by-step instructions on how to get the development environment up and running.

### Prerequisites
Before you begin, ensure you have the following prerequisites:
- .NET SDK (version 6.0 or higher)
- Git

### Steps
Clone the repository:

```
git clone https://gitlab.com/AlejandroDeOliveira/twitter-like-web-api.git
cd twitter-like-web-api
```

### Restore dependencies:
```
dotnet restore
```

### Build the project:
```
dotnet build
```

## Usage
Instructions on how to use the software. Provide examples and screenshots if applicable.

### Running the Application
To run the application, use the following command:

```
dotnet run --project path/to/the/project.csproj
```

I have not created any script to initialize both projects simultaneously. Yet, you can navigate to their directories manually and run this command.
Remember that the API Project must be running first so the CLI application can be functional.

### Example
In the next example we can see how all use cases are covered and functional. You may try doing them by yourself:
![](docs/Images/Requirements/Results.png)

## Running the Tests
The project implemented unit tests, these are the reports about test coverage (created by Coverlet)

![](docs/Images/Requirements/TestsCreated.png)
![](docs/Images/Requirements/CoverletSummary.png)
![](docs/Images/Requirements/TestCoverageOverview.png)

To run the unit tests, use the following command:
```
dotnet test
```

## License
This project is licensed under the MIT License.

## Contact
If you have any questions, feel free to contact me:

Alejandro Jose De Oliveira Barrancos  
Linkedin:  alejandrojosedeoliveirabarrancos  
Gmsil: alejandrojose.deoliveira@gmail.com

Project Link: https://gitlab.com/AlejandroDeOliveira/twitter-like-web-api