# Project Requirements Specifications Document

## Introduction
### Purpose
The purpose of this document is to specify the requirements for developing a console-based social networking application, similar to Twitter, which allows users to post messages and follow other users. The application will demonstrate basic functionalities such as posting messages, following users, and viewing a personal dashboard. Additionally, the project will adhere to the principles of Hexagonal Architecture to ensure a clean, maintainable, and testable codebase.

### Scope
This project aims to create a minimal viable product (MVP) of a social networking application that operates via a console interface. The application will support basic operations including posting messages, following other users, and displaying posts from followed users. The project does not require persistent storage or complex error handling, focusing instead on demonstrating core functionality, design patterns, and the implementation of Hexagonal Architecture.

### Definitions, Acronyms, and Abbreviations
- MVP: Minimal Viable Product
- Console Application: A program that runs in a command-line interface (CLI) or terminal
- Post: A message published by a user
- Follow: The action of subscribing to another user's posts
- Dashboard: The interface where users see posts from those they follow
- Hexagonal Architecture: An architectural pattern that promotes a clear separation of concerns, dividing the application into loosely coupled components that interact through well-defined interfaces

### References
- None yet.

### Overview
This document provides detailed requirements for the social networking console application, including functional and non-functional requirements, use case scenarios, and sequence diagrams. It outlines the necessary components and interactions to build a simple yet functional social networking platform, while adhering to the principles of Hexagonal Architecture.

## Overall Description
### Product Perspective
This console application is a standalone product designed to emulate basic social networking functionalities. It does not integrate with external systems or databases and operates entirely within the console environment.

### Product Functions
- Post messages: Users can publish messages that are timestamped and associated with their username.
- Follow users: Users can follow other users to see their posts on their dashboard.
- View dashboard: Users can view posts from the users they follow, displayed in chronological order.

### User Classes and Characteristics
- General User: Any individual using the application to post messages and follow others. Users are assumed to have basic familiarity with command-line interfaces.

### Operating Environment
The application will run in a console or terminal environment on any system that supports .NET Core or .NET Framework, including Windows, Linux, and macOS.

### Design and Implementation Constraints
- Console-based input and output: The application must operate via a console interface.
- No external libraries: Usage of external frameworks or libraries beyond the system-provided .NET libraries is discouraged.
- No persistence: The application does not require database or disk storage persistence.
- Language: The application should be written in a .NET-supported language, with a preference for C#.
- Hexagonal Architecture: The application must be designed following Hexagonal Architecture principles, ensuring a clear separation between the core business logic and the external interfaces.

### Assumptions and Dependencies
- Users will always input valid commands.
- The .NET runtime environment is available on the user's machine.
- No network connectivity is required for the application to function.

## System Features
### Architectural Overview
The application will be built using Hexagonal Architecture, also known as Ports and Adapters architecture. This approach ensures that the core business logic (the application’s domain) is isolated from external concerns (like the user interface and data storage). The application will be divided into the following layers:
- Domain Layer: Contains the core business logic and entities.
- Application Layer: Manages application-specific logic, including use cases and service orchestration.
- Infrastructure Layer: Handles external interactions, such as user input/output, which in this case is the console interface.
Product Functions

#### Domain Layer:
- Defines entities, value objects, and business rules.
- Encapsulates core business logic independent of technical details.

#### Application Layer:
- Manages use cases and application services.
- Coordinates between the domain layer and infrastructure layer.

#### Infrastructure Layer:
- Implements adapters for user interface and other external systems.
- Facilitates communication between the core application and external interfaces (e.g., console input/output).

## External Interface Requirements
### User Interfaces
The application will have a command-line interface (CLI) where users can input commands and view output. This interface will be an adapter in the infrastructure layer, interacting with the core application through defined ports.

### Hardware Interfaces
No specific hardware interfaces are required beyond a standard input (keyboard) and output (display/terminal).

### Software Interfaces
The application will be built using .NET Core or .NET Framework, with no additional software dependencies.

### Communication Interfaces
No network communication interfaces are required.

## System Requirements

### Functional Requirements Specification
#### FR-001: Posting Messages
Users must be able to post messages that include their username, the message content, and a timestamp.
- Dependencies: None.
- Key Features:
  - Users can post messages in the format: post @username message
  - Messages include a timestamp of when they were posted.
- Criteria of Acceptance:
  - Users can post a message by entering the correct command format.
  - The posted message appears with the correct username and timestamp.

#### FR-002: Following Users
Users must be able to follow other users to see their posts.
- Dependencies: None.
- Key Features:
  - Users can follow others using the command: follow @follower @followee
  - Following relationships are maintained during the session.
- Criteria of Acceptance:
  - Users can follow another user by entering the correct command format.
  - Following relationships are correctly established and reflected in the dashboard.

#### FR-003: Viewing Dashboard
Users must be able to view posts from the users they follow in chronological order.
- Dependencies: None.
- Key Features:
  - Users can view their dashboard using the command: wall @username
  - The dashboard displays posts from followed users in chronological order.
- Criteria of Acceptance:
  - Users can view their dashboard by entering the correct command format.
  - The dashboard accurately displays posts from followed users in the correct order.

### Non-Functional Requirements Specification
#### NFR-001: Performance
The system should provide prompt responses to user commands.
- Dependencies: None.
- Key Features:
  - Fast processing of commands.
  - Minimal delay in displaying posts and updates.
- Criteria of Acceptance:
  - Commands should execute within 1 second.
  - The system should handle typical usage scenarios without noticeable delay.

#### NFR-002: Usability
The system should be easy to use and understand for users with basic command-line knowledge.
- Dependencies: None.
- Key Features:
  - Intuitive command formats.
  - Clear and concise output messages.
- Criteria of Acceptance:
  - Users can learn to use the system with minimal instructions.
  - The system provides meaningful feedback for all commands.

#### NFR-003: Reliability
The system should operate consistently and handle typical command inputs without errors.
- Dependencies: None.
- Key Features:
  - Consistent behavior for identical inputs.
  - Error-free operation under normal conditions.
- Criteria of Acceptance:
  - The system should have no crashes or unexpected behavior during typical usage.

#### NFR-004: Security and Privacy
The system should ensure that user data is handled securely and privately, even though no persistent storage is used.
- Dependencies: None.
- Key Features:
  - Secure handling of user input.
  - No unnecessary exposure of user data.
- Criteria of Acceptance:
  - User data is not accessible outside the session.
  - No security vulnerabilities in handling user commands.

#### NFR-005: Compatibility
The system should be compatible with .NET Core and .NET Framework environments.
- Dependencies: None.
- Key Features:
  - Compatibility with major operating systems (Windows, Linux, macOS).
  - No reliance on platform-specific features.
- Criteria of Acceptance:
  - The system runs without modification on supported .NET environments.
  - The system exhibits consistent behavior across different platforms.

## Use Cases Specification
![](Images/Diagrams/Use-Cases-Diagram.png)

### UC1: Posting Messages
#### Actors:
- User: The person using the console application to post messages.
#### Preconditions:
- The application is running in a console environment.
- The user is aware of the correct command format for posting a message.
#### Main Scenario:
- User: Enters the command to post a message: post @username message.
- System: Validates the command format.
- System: Records the message with the current timestamp.
- System: Displays a confirmation message: @username posted -> "message" @timestamp.
#### Postcondition:
- The message is stored in the session and available for display on the user�s dashboard and the dashboards of their followers.
#### Alternative Scenarios:
- None, as the system assumes valid command inputs.

### UC2: Following Users
#### Actors:
- User: The person using the console application to follow other users.
#### Preconditions:
- The application is running in a console environment.
- The user is aware of the correct command format for following another user.
#### Main Scenario:
- User: Enters the command to follow another user: follow @follower @followee.
- System: Validates the command format and ensures both usernames exist.
- System: Establishes the following relationship between the two users.
- System: Displays a confirmation message: @follower started following @followee.
#### Postcondition:
- The following relationship is stored in the session, and posts from the followed user will appear on the follower's dashboard.
#### Alternative Scenarios:
- User: Enters a follow command with a non-existing user: follow @follower @nonexistentUser.
- System: Displays an error message: User @nonexistentUser not found.

### UC3: Viewing Dashboard
#### Actors:
- User: The person using the console application to view their dashboard.
#### Preconditions:
- The application is running in a console environment.
- The user has followed at least one other user.
#### Main Scenario:
- User: Enters the command to view their dashboard: wall @username.
- System: Validates the command format.
- System: Retrieves posts from users followed by @username.
- System: Displays the posts in chronological order.
#### Postcondition:
- The user views the most recent posts from the users they follow.
#### Alternative Scenarios:
- None, as the system assumes valid command inputs.


## Sequence Diagrams
### Main Sequence Posting Messages
![](Images/Diagrams/Post-Sequence-Diagram.png)

#### Participants
- User
- Console Application
#### Process Flow
1. User enters the post command.
2. Application processes the command.
3. Application stores the message.
4. Application confirms the post.

#### Detailed Steps
1. User: post @username message
2. Console Application:
  - Validates the command.
  - Records the message with the current timestamp.
  - Displays confirmation: @username posted -> "message" @timestamp

### Main Sequence Following Users
![](Images/Diagrams/Follow-Sequence-Diagram.png)

#### Participants
- User
- Console Application
#### Process Flow
1. User enters the follow command.
2. Application processes the command.
3. Application establishes the follow relationship.
4. Application confirms the follow action.
#### Detailed Steps
1. User: follow @follower @followee
2. Console Application:
  - Validates the command.
  - Establishes the following relationship.
  - Displays confirmation: @follower started following @followee

### Main Sequence Viewing Dashboard
![](Images/Diagrams/Wall-Sequence-Diagram.png)

#### Participants
- User
- Console Application
#### Process Flow
1. User enters the wall command.
2. Application processes the command.
3. Application retrieves relevant posts.
4. Application displays the posts.
#### Detailed Steps
1. User: wall @username
2. Console Application:
  - Validates the command.
  - Retrieves posts from followed users.
  - Displays posts in chronological orde