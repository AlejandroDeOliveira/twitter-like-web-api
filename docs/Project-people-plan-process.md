# Software Development Project Documentation
## Introduction
### Purpose
The purpose of this document is to provide a comprehensive overview of the project plan, processes, people involved, and practices to be followed in developing a console-based social networking application, akin to Twitter. This application will allow users to post messages, follow other users, and view a personal dashboard, demonstrating basic functionalities and adhering to design patterns.

### Scope
This document outlines the necessary components and interactions to build a simple yet functional social networking platform. The project will focus on core functionalities and design patterns.

### Overview
This document details the project scope, objectives, schedule, roles, methodologies, and conventions that will guide the development of the social networking application. It serves as a reference for planning, execution, and evaluation throughout the project lifecycle.

## Project
### Objectives
- Develop a minimal viable product (MVP) of a console-based social networking application.
- Implement core functionalities: posting messages, following users, and viewing a dashboard.
- Ensure the application is easy to use, reliable, and performs well within the constraints of a console interface.
- Deliver the project by May 31, 2024, with specific milestones for development and testing.
### Conventions and Practices
- Coding Standards: Follow C# coding conventions and best practices as outlined in the .NET documentation.
- Version Control: Use Git for version control, with a clear branching strategy (master branch for stable releases, development branch for ongoing work, feature branches for specific functionalities).
- Documentation: Maintain comprehensive documentation for all code and processes, including inline comments, README files, and API documentation.
- Testing: Implement unit tests for all core functionalities using a suitable testing framework like xUnit. Ensure thorough test coverage and regular testing throughout the development cycle.

## Plan
### Timeline and Milestones
The project must be completed by May 31, 2024. Key milestones are as follows:

- May 28, 2024: Complete the MVP with basic functionalities (posting messages, following users, viewing dashboard).
- May 29, 2024: Finish writing and executing test cases to ensure all functionalities work as expected.
- May 30, 2024: Address any minor details and issues identified during testing.
- May 31, 2024: Enhance the application by adding value through additional functionalities or covering non-functional requirements (e.g., performance improvements, better usability).

### Detailed Schedule
- May 25-26, 2024: Setup project environment, initialize version control, and outline initial structure.
- May 27-28, 2024: Develop core functionalities (posting, following, viewing dashboard).
- May 28, 2024: Initial internal testing and debugging.
- May 28, 2024: Finalize MVP and prepare for comprehensive testing.
- May 29, 2024: Execute and validate test cases.
- May 30, 2024: Resolve identified issues, optimize performance, and ensure usability.
- May 31, 2024: Implement additional features or improvements, finalize documentation, and prepare for project presentation.

## People
### Roles and Responsibilities
Software Development Student and Applicant: As the sole developer, I am responsible for all aspects of the project, including design, coding, testing, and documentation. My experience with C# and eagerness to learn will drive the project's success.
### Experience and Skills
- Experience: Proficient in C# with hands-on experience in software development projects.
- Skills: Strong understanding of .NET framework, object-oriented programming, and command-line interface development. Capable of writing clean, maintainable code and conducting thorough testing.

## Process
### Methodology
The project will follow an Agile methodology to ensure flexibility, iterative development, and continuous improvement. Key aspects include:
- Daily Stand-ups: Brief daily meetings (even if just personal notes) to review progress, identify obstacles, and plan the day's work.
- Retrospectives: Reflect on what went well, what could be improved, and implement changes for continuous improvement.  
Iterations and some other ceremonies are not ment to be applied due to the reduced development time and milestones.

### Agile Roles and Rules
- Scrum Master: As the sole developer, I will also fulfill the role of Scrum Master, ensuring adherence to Agile principles and facilitating smooth progress.
- Product Owner: I will act as the Product Owner, defining the vision, managing the backlog, and ensuring the project meets its objectives.
- Development Team: I am responsible for implementing the functionalities, conducting tests, and maintaining documentation.