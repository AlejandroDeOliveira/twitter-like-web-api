### Api Ports Design Decision Record
Short descriptive title of the decision.
 
## Status
Accepted
 
## Context
The project aims to develop a console-based social networking application that allows users to post messages, follow other users, and view their dashboard, following the principles of Hexagonal Architecture. The application will use .NET Core or .NET Framework and should be compatible with major operating systems, requiring no persistent storage. The design decision focuses on defining API ports for managing users and walls, which are central to the application's functionality.

## Decision
The decision is to implement two primary API controllers: UserController and WallController. Each controller will handle specific aspects of the application's functionality related to user management and wall management, respectively.

### UserController
Responsibilities:
- Retrieve user details.
- Add new users.
- Edit existing users.

### WallController
Responsibilities:
- Post messages to a user's wall.
- Follow other users.
- Retrieve the wall for a user.

The decision was made to structure the controllers in this way to ensure a clear separation of concerns, enhance maintainability, and adhere to Hexagonal Architecture principles.

## Alternatives Considered

### Alternative 1: Monolithic Controller
Use a single controller to manage all user and wall-related functionalities.
- Pros:
  - Simplified routing and configuration.
  - Easier to implement initially.
- Cons:
  - Poor separation of concerns.
  - Harder to maintain and extend.
  - Violates Hexagonal Architecture principles.

This approach does not align with the project's architectural goals and would lead to a less maintainable codebase.

### Alternative 2: Multiple Microservices
Split the application into multiple microservices, each handling different aspects of user and wall management.
- Pros:
  - High scalability and independence of services.
  - Each service can be developed, deployed, and scaled independently.
- Cons:
  - Increased complexity in managing multiple services.
  - Higher overhead in terms of deployment and inter-service communication.

The project scope does not require the complexity of microservices. A console-based application with no persistence needs can be efficiently managed with a simpler structure.

## Consequences
### Positive Impacts
- Separation of Concerns: The decision to have dedicated controllers for users and walls ensures a clear separation of responsibilities, making the codebase more modular and easier to maintain.
- Adherence to Hexagonal Architecture: This design aligns with the principles of Hexagonal Architecture, promoting a clean and maintainable code structure.
- Scalability: While not a primary concern for the console application, the separation of concerns makes it easier to scale or refactor specific parts of the application in the future.
### Negative Impacts
- Initial Complexity: Implementing and managing multiple controllers might introduce initial complexity compared to a single monolithic controller.

## Implementation Plan
- Define Interfaces: Create interfaces in the Application.Ports namespace to abstract the operations required by the UserController and WallController.
- Implement Controllers: Implement the UserController and WallController in the Api.Controllers namespace.
Define the routes and HTTP actions for each controller.
- Service Layer: Implement services in the Application layer to handle business logic for users and walls.
Ensure these services adhere to the defined interfaces.
- Dependency Injection: Configure dependency injection in the Startup class to inject the appropriate services into the controllers.
- Testing: Develop unit tests and integration tests to ensure each controller and its actions work as expected.
Conduct thorough testing to validate the end-to-end functionality of the API. 

## Related Decisions
- Decision on Hexagonal Architecture for the Console-Based Social Networking Application.
- Decision on .NET Core as the Framework for Development. 
- Specific and functional requirements documentation.

## References
- [Clean Architecture: A Craftsman's Guide to Software Structure and Design](https://play.google.com/store/books/details/Robert_C_Martin_Clean_Architecture?id=uGE1DwAAQBAJ&pli=1)
- [Hexagonal Architecture Design Pattern](https://www.mitrais.com/news-updates/hexagonal-architecture-design-pattern/)

## Date
May 31, 2024

## Author(s)
Alejandro Jose De Oliveira