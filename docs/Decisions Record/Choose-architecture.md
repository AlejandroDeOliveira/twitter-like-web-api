### Hexagonal Architecture Selection Decision Record
## Status
Accepted
 
## Context
The project aims to develop a console-based social networking application similar to Twitter, with core functionalities including posting messages, following users, and viewing a personal dashboard. Given the project's requirements to demonstrate core functionality without persistent storage or complex error handling, and the need for a maintainable, testable, and scalable architecture, Hexagonal Architecture (HA) was considered. Hexagonal Architecture promotes a clean separation of concerns, making it easier to manage changes, add features, and conduct unit testing. The decision to adopt HA was influenced by the desire to ensure that the core business logic remains independent of external interfaces, facilitating future extensions or modifications.
 
## Decision
The project will adopt Hexagonal Architecture. This decision was made to ensure that the application remains modular, maintainable, and testable. Hexagonal Architecture will provide a clear separation between the core business logic and external components, such as the user interface. This architectural pattern will support the project's need for a clean and efficient design, making it easier to manage and evolve the application over time.
 
## Alternatives Considered
### Monolithic Architecture
A single-tiered software application in which different components are combined into a single program.
- Pros:
  - Simplicity in design and implementation.
  - Faster initial development time.
- Cons:
  - Difficult to maintain and scale.
  - Tight coupling of components makes testing and modification challenging.
  - Reasons for not choosing: The lack of modularity and difficulty in maintaining and scaling the application were significant drawbacks. Additionally, testing would be cumbersome due to tightly coupled components.
### Layered (N-Tier) Architecture
A software architecture that separates the application into distinct layers (e.g., presentation, business logic, data access).
- Pros:
  - Clear separation of concerns.
  - Easier to maintain and test compared to monolithic architecture.
- Cons:
  - Can still lead to tight coupling between layers.
  - Changes in one layer might affect others.
  - Reasons for not choosing: While this architecture offers better separation of concerns than a monolithic design, it still does not provide the flexibility and independence between components that Hexagonal Architecture offers.
### Microservices Architecture
An approach where the application is developed as a collection of loosely coupled services.
- Pros:
  - High scalability and flexibility.
  - Independent deployment and development of services.
- Cons:
  - Increased complexity in development and deployment.
  - Requires robust infrastructure and DevOps practices.
  - Reasons for not choosing: Given the project’s scope and the absence of a need for complex deployment and scaling requirements, this architecture would introduce unnecessary complexity.

## Consequences
### Positive Impacts
- Modularity: The application will be divided into distinct components, each responsible for a specific aspect of the system, improving maintainability.
- Testability: The clear separation between business logic and external components facilitates unit testing and mocking of dependencies.
- Scalability: Future enhancements or modifications can be easily integrated without significant refactoring.
- Maintainability: Changes in one part of the application will have minimal impact on others, simplifying updates and bug fixes.

### Negative Impacts
- Initial Complexity: Implementing Hexagonal Architecture might be more complex initially compared to simpler architectures like monolithic or layered architectures.
- Learning Curve: Developers may need to familiarize themselves with the principles and best practices of Hexagonal Architecture, which could require additional training or research time.
 
## Implementation Plan
- Initial Design Phase: Outline the core components, including domain, application, and infrastructure layers. Define interfaces (ports) and implement initial adapters.
Timeline: 1 day.

- Incremental Development: Develop the application incrementally, starting with core functionalities (posting messages, following users, viewing dashboard) while adhering to Hexagonal Architecture principles.
Timeline: 2 days.

- Testing: Implement unit tests for the core business logic, ensuring that all components interact correctly through defined interfaces.
Timeline: Ongoing during development
 
## Related Decisions
- Requirement on Using .NET Core  
The decision to use .NET Core as the development framework was influenced by its strong support for clean architecture practices, including Hexagonal Architecture. .NET Core provides robust tools and libraries that facilitate the implementation of modular and testable code structures, aligning well with the principles of Hexagonal Architecture. This requirement ensures that the application can leverage the cross-platform capabilities of .NET Core, making it compatible with various operating systems (Windows, Linux, macOS) and ensuring a broad deployment base.

- Requirement on Console Interface  
The requirement to implement a console-based user interface significantly impacts the design of the infrastructure layer in Hexagonal Architecture. This interface will serve as an adapter, interacting with the core business logic through defined ports. The console interface requirement emphasizes the need for a clear separation between the user interface and the business logic, ensuring that the application remains flexible and maintainable. This separation allows for future changes or additions to the user interface (e.g., web or mobile interfaces) without affecting the core business logic.
 
## References
- [.NET fundamentals documentation](https://learn.microsoft.com/en-us/dotnet/fundamentals/)
- [Architecture: Clean, Onion, Hexagonal vs MVVM, MVC, MVP.](https://www.reddit.com/r/csharp/comments/ajuizs/architecture_clean_onion_hexagonal_vs_mvvm_mvc_mvp/)
- [Hexagonal Architecture, there are always two sides to every story](https://medium.com/ssense-tech/hexagonal-architecture-there-are-always-two-sides-to-every-story-bc0780ed7d9c)
- [Hexagonal architecture](https://alistair.cockburn.us/hexagonal-architecture/)
- [EXAGONAL ARCHITECTURE - WHAT IS IT? WHY SHOULD YOU USE IT?](https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/)


## Date
26/05/2024

## Author(s)
Alejandro Jose De Oliveira Barrancos