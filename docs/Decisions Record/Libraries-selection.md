### Libraries Selection Decision Record
## Status
Accepted

## Context
The project aims to develop a console-based social networking application similar to Twitter, focusing on core functionalities like posting messages, following users, and viewing a personal dashboard. The project follows Hexagonal Architecture principles to ensure modularity, maintainability, and testability. While the project guidelines discourage the use of external libraries, the need for efficient testing and maintainability prompts the consideration of **xUnit** for unit testing and **Microsoft.AspNetCore.Mvc.Testing** for integration testing.
 
## Decision
The project will adopt xUnit as the unit testing framework and Moq for mocking dependencies. This decision is based on the following factors:
- xUnit is a widely-used and well-supported testing framework in the .NET ecosystem. Its simplicity, extensibility, and integration with Visual Studio make it a suitable choice for writing and executing unit tests.
- Microsoft.AspNetCore.Mvc.Testing provides infrastructure for integration testing ASP.NET Core MVC applications. It allows for more realistic testing scenarios by hosting the application in-memory and sending HTTP requests to the hosted application, thereby facilitating end-to-end testing. 

## Alternatives Considered
### No External Libraries
- Pros:
  - Minimal dependencies, reducing potential compatibility issues and project complexity.
- Cons:
  - Limited testing capabilities and increased manual effort in writing test code.
  - Difficulty in mocking dependencies, leading to less effective unit testing.
  - Reasons for not choosing it: Given the project's emphasis on maintainability and testability, avoiding external libraries would compromise the efficiency and effectiveness of testing, hindering the project's overall quality.

## Consequences
### Positive Impacts
- Improved Test Coverage: xUnit and Microsoft.AspNetCore.Mvc.Testing enable comprehensive unit and integration testing, ensuring robustness and reliability of the application.
- Enhanced Maintainability: Unit and integration tests facilitate code refactoring and modification by providing a safety net against regressions.
- Faster Development: Using established testing frameworks accelerates the testing process, allowing developers to iterate more quickly.

### Negative Impacts
- Increased Dependency: Introducing external libraries adds dependencies to the project, potentially complicating dependency management and version control.
 
## Implementation Plan
1. Integration of xUnit and Microsoft.AspNetCore.Mvc.Testing: Include xUnit and Microsoft.AspNetCore.Mvc.Testing packages in the project dependencies using NuGet Package Manager or .NET CLI.
2. Test Setup: Create test projects alongside the application solution and organize test classes corresponding to the application's components.
3. Write Unit Tests: Develop unit tests using xUnit to cover critical functionalities and edge cases.
4. Write Integration Tests: Implement integration tests using Microsoft.AspNetCore.Mvc.Testing to test the behavior of API endpoints and their interactions with the application.
5. Execute Tests: Run unit and integration tests regularly during development and integrate automated testing into the CI/CD pipeline.
6. Review and Refine: Continuously review and refine tests to maintain alignment with code changes and evolving requirements.

## Related Decisions
- Requirement on Using .NET Core  
The decision to use .NET Core as the development framework was influenced by its strong support for clean architecture practices, including Hexagonal Architecture. .NET Core provides robust tools and libraries that facilitate the implementation of modular and testable code structures, aligning well with the principles of Hexagonal Architecture. This requirement ensures that the application can leverage the cross-platform capabilities of .NET Core, making it compatible with various operating systems (Windows, Linux, macOS) and ensuring a broad deployment base.
- [Hexagonal Architecture desicion record.](https://gitlab.com/AlejandroDeOliveira/twitter-like-web-api/-/blob/doc/project-documentation/docs/Decisions%20Record/Choose-architecture.md?ref_type=heads)
 
## References
- [About xUnit.net.](https://xunit.net/)
- [.NET fundamentals documentation.](https://learn.microsoft.com/en-us/dotnet/fundamentals/)
- [Integration tests in ASP.NET Core](https://learn.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-6.0) 

## Date
26/05/2024

## Author(s)
Alejandro Jose De Oliveira Barrancos