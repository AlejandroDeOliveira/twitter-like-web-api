# Security

No security measures have been applied but **HTTPS** protocols have been followed and basic sensitive data (like usernames) were avoided to be passed by the requests' body.